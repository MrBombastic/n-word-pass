# N-Word Pass

To make the world a better place and help fight censorship this addon will replace some instances of the silly `n-word` with its proper counterpart.

## Download
### AMO:
Coming soon hopefully.
### Direct:
[Click here](n_word_pass-1.0.0-an+fx.xpi)


This addon is based on [xi-jinping-to-winnie-the-pooh](https://github.com/abc157864/xi-jinping-to-winnie-the-pooh), which is itself a fork of [B!tch to Boss](https://addons.mozilla.org/en-US/firefox/addon/b-itch-to-boss/) by Mozilla Firefox, licensed under Mozilla Public License, version 2.0 (http://www.mozilla.org/MPL/2.0/).

![Screenshot n-word duckduckgo](screenshot_n_word.png)
N-Word on DuckDuckGo.
